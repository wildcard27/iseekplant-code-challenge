<?php

use App\Http\Controllers\WeatherController;
use Illuminate\Support\Facades\Route;

Route::apiResource('weather/{latitude}/{longitude}', WeatherController::class)->only('index');
