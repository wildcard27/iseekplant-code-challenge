<?php

namespace Tests\Unit\Models;

use App\Models\Weather;
use PHPUnit\Framework\TestCase;

class WeatherTest extends TestCase
{

    /** @var Weather */
    private Weather $weatherModel;

    public function setUp(): void
    {
        $this->weatherModel = new Weather();
    }

    public function testDay()
    {
        $this->weatherModel->setDay("Tuesday");

        $this->assertEquals("Tuesday", $this->weatherModel->getDay());
    }

    public function testTempMin()
    {
        $this->weatherModel->setTempMin(10);

        $this->assertEquals(10, $this->weatherModel->getTempMin());
    }

    public function testTempMax()
    {
        $this->weatherModel->setTempMax(20.612);

        $this->assertEquals(20.612, $this->weatherModel->getTempMax());
    }

    public function testRain()
    {
        $this->weatherModel->setRain(12.123);

        $this->assertEquals(12.123, $this->weatherModel->getRain());
    }

    public function testDescription()
    {
        $this->weatherModel->setDescription('Light rain');

        $this->assertEquals('Light rain', $this->weatherModel->getDescription());
    }

    public function testIcon()
    {
        $this->weatherModel->setIcon('weather-icon');

        $this->assertEquals('weather-icon', $this->weatherModel->getIcon());
        $this->assertEquals(
            'https://openweathermap.org/img/wn/weather-icon@2x.png',
            $this->weatherModel->getIconUrl()
        );
    }

    public function testUvIndex()
    {
        $this->weatherModel->setUvIndex('5');

        $this->assertEquals(5, $this->weatherModel->getUvIndex());
    }
}
