<?php

namespace Tests\Unit\Models;

use App\Models\LatLong;
use PHPUnit\Framework\TestCase;

class LatLongTest extends TestCase
{

    /** @var LatLong */
    private LatLong $latLongModel;

    protected function setUp(): void
    {
        $this->latLongModel = new LatLong();
    }

    public function testLatitude()
    {
        $this->latLongModel->setLatitude('-41.123456');

        $this->assertEquals('-41.123456', $this->latLongModel->getLatitude());
    }

    public function testLongitude()
    {
        $this->latLongModel->setLongitude('153.654321');

        $this->assertEquals('153.654321', $this->latLongModel->getLongitude());
    }

    public function testToString()
    {
        $this->latLongModel->setLatitude('-41.123456')
            ->setLongitude('153.654321');

        $this->assertEquals('-41.123456,153.654321', $this->latLongModel->toString());
    }

    public function testToStringNull()
    {
        $this->assertEquals("", $this->latLongModel->toString());
    }
}
