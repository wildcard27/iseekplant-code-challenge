const API_URL = 'http://127.0.0.1:8000/api/weather';

// Fetch weather from API
const getForecast = async (lat, long) => {

    return await fetch(`${API_URL}/-27.78333/153.26666562`)
        .then(response => response.text())
        .then(result => {
            return result
        })
        .catch(error => console.log('error', error));
}

export default (getForecast);
