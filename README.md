# iSeekPlant Code Challenge

## Prerequisites

In order to install and run this application locally, you will need to ensure your environment has the following
dependencies:

- PHP 7.4 (Previous versions untested)
- An API key from https://openweathermap.org/
- A Google Maps API key from [here](https://console.cloud.google.com/marketplace/details/google/maps-backend.googleapis.com?filter=category:maps&id=fd73ab50-9916-4cde-a0f6-dc8be0a0d425)

## Installation

1. Rename `.env.example` to `.env` and add in you OpenWeatherMap API key

2. Clone this repository and install composer dependencies

```shell
$ composer install
```

3. Install front end dependencies

```shell
$ npm install
```

4. Build front end

```shell
$  npm run {dev|prod}
```

5. Profit.

## Running locally

To run the application using PHP's built-in server, run

```shell
$ php artisan serve
```

This will start the server and will give you a local URL to visit the compiled site.

## Endpoints

#### Get 5-day weather forecast

`/api/weather/{latitude}/{longitude}`

#### Using front end
`http://127.0.0.1:8000/`
