import React, { useState } from 'react';
import ReactGoogleAutocomplete from 'react-google-autocomplete';
import getForecast from '../http/weather';

const AddressSearch = () => {
    const apiKey = process.env.MIX_GOOGLE_API_KEY;
    const [location, setLocation] = useState({ isLoading: false, result: null });

    const handleSelect = (place) => {
        setLocation({ isLoading: true, result: null })
        const lat = place.geometry.location.lat;
        const lng = place.geometry.location.lng;

        getForecast(lat, lng)
            .then((result) => {
                setLocation({ isLoading: false, result: result })
            });
    }

    return (
        <>
            <ReactGoogleAutocomplete
                apiKey={apiKey}
                onPlaceSelected={(place) => handleSelect(place)}
            />
            <div>
                {
                    !location.isLoading &&

                    <div>{location.result}</div>
                }
            </div>
        </>
    )
};

export default AddressSearch;
