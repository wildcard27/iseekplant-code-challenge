<?php


namespace App\Exceptions;


use App\Services\WeatherService;
use Throwable;

class WeatherApiKeyNotFoundException extends \Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $apiKeyName = WeatherService::API_KEY_NAME;
        $message .= "Weather API key not found. Please ensure $apiKeyName is set in your environment variables";

        parent::__construct($message, $code, $previous);
    }
}
