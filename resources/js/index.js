import React from 'react';
import ReactDOM from 'react-dom';
import Home from './pages/home';

function Index() {
    return (
        <Home />
    );
}

export default Index();

ReactDOM.render(<Index />, document.getElementById('app'));
