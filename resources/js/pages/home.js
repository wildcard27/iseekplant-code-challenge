import React from 'react';
import AddressSearch from '../components/AddressSearch';

const Home = () => {
    return (
        <div>
            <AddressSearch />
        </div>
    );
};

export default Home;
