<?php

namespace App\Http\Controllers;

use App\Models\LatLong;
use App\Services\WeatherService;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Response;

class WeatherController extends Controller
{

    /** @var WeatherService */
    protected WeatherService $weatherService;

    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    /**
     * Get weather for city
     *
     * @param string $latitude
     * @param string $longitude
     *
     * @return Response
     */
    public function index(string $latitude, string $longitude): Response
    {
        $latLong = new LatLong();
        $latLong->setLatitude($latitude)
            ->setLongitude($longitude);

        try {
            $forecast = $this->weatherService->getForecast($latLong);
        } catch (RequestException $exception) {
            return new Response($exception->getMessage(), 400);
        }

        return new Response($forecast);
    }
}
