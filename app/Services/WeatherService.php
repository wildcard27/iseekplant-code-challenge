<?php

namespace App\Services;

use App\Exceptions\WeatherApiKeyNotFoundException;
use App\Models\LatLong;
use App\Models\Weather;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class WeatherService
{

    const API_KEY_NAME = "OPENWEATHER_API_KEY";
    const API_URL = "https://api.openweathermap.org/data/2.5/onecall";
    const NUMBER_OF_DAYS_FORECAST = 5;

    /** @var string */
    private string $apiKey;

    /**
     * @throws WeatherApiKeyNotFoundException
     */
    public function __construct()
    {
        $this->apiKey = env(self::API_KEY_NAME);

        if (!$this->apiKey) {
            throw new WeatherApiKeyNotFoundException();
        }
    }

    /**
     * @return Weather[]
     * @throws RequestException
     */
    public function getForecast(LatLong $latLong): array
    {
        $response = Http::get(self::API_URL, $this->getUrlParams($latLong));

        $response->throw()->json();

        $forecast = [];
        $count = 0;
        foreach ($response->json('daily') as $day) {
            $count++;

            $weather = new Weather();
            $weather->setDay(date('l', $day['dt']))
                ->setTempMin($day['temp']['max'])
                ->setTempMax($day['temp']['min'])
                ->setRain($day['rain'] ?? 0)
                ->setUvIndex($day['uvi'] ?? 0)
                ->setDescription($day['weather'][0]['description'])
                ->setIcon($day['weather'][0]['icon']);

            $forecast[] = $weather;

            if ($count === self::NUMBER_OF_DAYS_FORECAST) {
                break;
            }
        }

        return $forecast;
    }

    /**
     * @param LatLong $latLong
     *
     * @return array
     */
    private function getUrlParams(LatLong $latLong): array
    {
        return [
            'appid' => $this->apiKey,
            'lat' => $latLong->getLatitude(),
            'lon' => $latLong->getLongitude(),
            'exclude' => "current,minutely,hourly,alerts",
            'units' => "metric",
        ];
    }
}
