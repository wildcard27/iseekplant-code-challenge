<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{

    const ICON_URL = "https://openweathermap.org/img/wn/%s@2x.png";

    /** @var string */
    private string $day;

    /** @var float */
    private float $tempMin;

    /** @var float */
    private float $tempMax;

    /** @var float */
    private float $rain;

    /** @var string */
    private string $description;

    /** @var string */
    private string $icon;

    /** @var int */
    private int $uvIndex;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return string
     */
    public function getDay(): string
    {
        return $this->day;
    }

    /**
     * @param string $day
     *
     * @return Weather
     */
    public function setDay(string $day): self
    {
        $this->day = $day;

        return $this;
    }

    /**
     * @return float
     */
    public function getTempMin(): float
    {
        return $this->tempMin;
    }

    /**
     * @param float $tempMin
     *
     * @return Weather
     */
    public function setTempMin(float $tempMin): self
    {
        $this->tempMin = $tempMin;

        return $this;
    }

    /**
     * @return float
     */
    public function getTempMax(): float
    {
        return $this->tempMax;
    }

    /**
     * @param float $tempMax
     *
     * @return Weather
     */
    public function setTempMax(float $tempMax): self
    {
        $this->tempMax = $tempMax;

        return $this;
    }

    /**
     * @return float
     */
    public function getRain(): float
    {
        return $this->rain;
    }

    /**
     * @param float $rain
     *
     * @return Weather
     */
    public function setRain(float $rain): self
    {
        $this->rain = $rain;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Weather
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return Weather
     */
    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getIconUrl(): string
    {
        return $this->icon ? sprintf(self::ICON_URL, $this->icon) : "";
    }

    /**
     * @return int
     */
    public function getUvIndex(): int
    {
        return $this->uvIndex;
    }

    /**
     * @param int $uvIndex
     *
     * @return Weather
     */
    public function setUvIndex(int $uvIndex): self
    {
        $this->uvIndex = $uvIndex;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'day' => $this->getDay(),
            'tempMin' => $this->getTempMin(),
            'tempMax' => $this->getTempMax(),
            'rain' => $this->getRain(),
            'description' => $this->getDescription(),
            'icon' => $this->getIconUrl()
        ];
    }
}
