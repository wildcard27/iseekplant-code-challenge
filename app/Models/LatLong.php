<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LatLong extends Model
{

    /** @var string */
    private string $latitude;

    /** @var string */
    private string $longitude;

    /**
     * @return string
     */
    public function getLatitude(): string
    {
        return $this->latitude;
    }

    /**
     * @param string $latitude
     *
     * @return LatLong
     */
    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongitude(): string
    {
        return $this->longitude;
    }

    /**
     * @param string $longitude
     *
     * @return LatLong
     */
    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return string
     */
    public function toString(): ?string
    {
        if (!isset($this->latitude) || !isset($this->longitude)) {
            return "";
        }

        return "$this->latitude,$this->longitude";
    }
}
