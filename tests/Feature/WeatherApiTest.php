<?php

namespace Tests\Feature;

use Tests\TestCase;

class WeatherApiTest extends TestCase
{

    public function testWeatherEndpointExists()
    {
        $response = $this->get('/api/weather/-27/153');

        $response->assertStatus(200);
    }

    public function testRequiresCity()
    {
        $response = $this->get('/api/weather');

        $response->assertStatus(404);
    }
}
